class Comp < Player
  # attr_accessor :name, :symbol, :scores

  NAMES = [
    "Frank Sinatra",
    "Pericles",
    "Jenna Jameson",
    "Ron Jeremy",
    "Barack Obama"
  ]

  def initialize player_symbol
    @name = pick_appropriate_name(player_symbol, NAMES)
    @symbol = @name[0]
    @scores = { wins: 0, draws: 0 }
  end

  def move table, player
    chosen_slot = find_critical_slot_in_(table, player)
    table.slots[chosen_slot[0]][chosen_slot[1]] = self
    table.to_s
    chosen_slot
  end

  private

  def pick_appropriate_name symbol, names
    if names.inject([]) { |syms, nm| syms << nm.split(//).first }.include? symbol
      names.reject { |name| name[0] == symbol }[rand(0...names.length)]
    else
      names[rand(0...names.length)]
    end
  end

  def find_critical_slot_in_ table, player
    ids = [:comp, :player]
    chosen_slot = nil
    critical_lines = {
      comp: [],
      player: []
    }

    critical_rows = collect_rows_or_cols_in(table, player)
    critical_cols = collect_rows_or_cols_in(table, player, {:collect_columns => true})
    critical_cols = collect_rows_or_cols_in(table, player, {:collect_columns => true})
    critical_dgs = collect_diagonals_in(table, player)

    ids.each do |id|
      critical_rows[id].each { |slot| critical_lines[id] << slot }
      critical_cols[id].each { |slot| critical_lines[id] << slot }
      critical_dgs[id].each { |slot| critical_lines[id] << slot }
      critical_lines[id].uniq! unless critical_lines.empty?
    end

    comp_cls = critical_lines[:comp]
    player_cls = critical_lines[:player]
    comp_rnd = rand(0...comp_cls.length) unless comp_cls.empty?
    player_rnd = rand(0...player_cls.length) unless player_cls.empty?

    if !comp_cls.empty?
      chosen_slot = comp_cls[comp_rnd]
    elsif !player_cls.empty?
      chosen_slot = player_cls[player_rnd]
    else
      chosen_slot = find_random_slot(table)
    end
    chosen_slot
  end

  def collect_rows_or_cols_in table, player, options = {:collect_columns => false}
    table_columns = [] if options[:collect_columns]
    table = table.slots
    critical_lines = {
      comp: [],
      player: []
    }

    if options[:collect_columns]
      0..table.length.times do |x|
        column = []
        0..table.length.times do |y|
          column << table[y][x]
        end
        table_columns << column
      end
      table = table_columns
    end

    table.each_with_index do |row, x|
      player_score = 0
      comp_score = 0
      nil_cds = []
      nils = 0

      row.each_with_index do |slot, y|
        break if (!player_score.zero? && !comp_score.zero?) || nils > 1
        if options[:collect_columns]
          nil_cds << y << x if slot.nil?
        else
          nil_cds << x << y if slot.nil?
        end

        if slot == player
          player_score += 1
        elsif slot == self
          comp_score += 1
        else
          nils += 1
        end

        if player_score == table.size - 1 && nils == 1
          critical_lines[:player] << nil_cds
        elsif comp_score == table.size - 1 && nils == 1
          critical_lines[:comp] << nil_cds
        end
      end
    end
    critical_lines
  end

  def collect_diagonals_in table, player
    dgs = []
    table = table.slots
    critical_diagonals = {
      comp: [],
      player: []
    }

    dgs << table.size.times.inject([]) { |tld, n| tld << [[n,n], table[n][n]] }
    dgs << table.size.times.inject([]) { |bld, n| bld << [[n, (table.size - 1) -n], table[n][(table.size - 1) - n]] }

    dgs.each do |diagonal|
      player_score = 0
      comp_score = 0
      nil_cds = []
      nils = 0

      diagonal.each do |slot|
        break if (!player_score.zero? && !comp_score.zero?) || nils > 1
        if slot[1].nil?
          nil_cds << slot[0];
          nil_cds.flatten!
        end

        if slot[1] == player
          player_score += 1
        elsif slot[1] == self
          comp_score += 1
        else
          nils += 1
        end

        if player_score == table.size - 1 && nils == 1
          critical_diagonals[:player] << nil_cds
        elsif comp_score == table.size - 1 && nils == 1
          critical_diagonals[:comp] << nil_cds
        end
      end
    end
    critical_diagonals
  end

  def find_random_slot table
    nil_slots = []
    table = table.slots
    table.size.times do |x|
      table.size.times do |y|
        nil_slots << [x, y] if table[x][y].nil?
      end
    end
    nil_slots[rand(0...nil_slots.size)]
  end
end
