class Player
  attr_reader :name, :symbol, :scores

  def initialize name, symbol
    @name = name
    @symbol = symbol
    @scores = { wins: 0, draws: 0 }
  end

  def move table
    rows = table.slots.length
    slots = table.slots.length ** 2
    loop do
      puts
      puts "Choose a correct (1-#{slots}) slot:".right_adjust($padding)
      print "-> ".right_adjust($padding-3)
      chosen_slot = gets.chomp.to_i - 1
      puts
      if chosen_slot >= 0 && chosen_slot <= slots - 1
        slot = [chosen_slot/rows, chosen_slot%rows]
        if table.slots[slot.first][slot.last].nil?
          table.slots[slot.first][slot.last] = self
          table.to_s
          return slot
        else
          puts "Slot you chose is already filled!".right_adjust($padding)
        end
      else
        puts "Invalid slot position!".right_adjust($padding)
      end
    end
  end
  
  def increment_win
    @scores[:wins] += 1
  end

  def increment_draw
    @scores[:draws] += 1
  end
end
