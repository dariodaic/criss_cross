require './game'
require './player_input'
require './player'
require './comp'
require './table'

player = Player.new PlayerInput.choose_player_name, PlayerInput.choose_player_symbol
comp = Comp.new player.symbol
Game.new(player, comp, PlayerInput.choose_table_width).play_new