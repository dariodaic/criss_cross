$padding = 50

class String
  def right_adjust distance, *varl
    if distance >= self.length/2
      " " * (distance - (self.length/2)) + self
    else
      puts "> DEBUG INFO <"
      puts "Half of your longest sentence is longer than your chosen padding."
      puts "Increase padding...or do something else."
      exit
    end
  end
end

class Game
  @@games_time = []

  class <<self
    def game_time
      mins, secs = count_game_time(game_time_sum)
      puts "You  wasted #{mins} min and #{secs} sec on this game.".right_adjust($padding)
      puts "Go and play Starcraft now.".right_adjust($padding)
    end

    def game_time_sum
      @@games_time.each.inject(0) { |time_sum, sec| time_sum += sec }
    end

    def count_game_time time
      [time.to_i/60, time.to_i%60]
    end
  end

  attr_reader :player, :comp, :start_time, :table

  def initialize player, comp, table_width
    @player = player
    @comp = comp
    @table = Table.new table_width
    @start_time = Time.now
  end

  def play_new
    game_over = false

    display_stats @player, @comp

    loop do
      if scored? player.move @table
        proclaim_winner @player
        @@games_time << Time.now - @start_time
        game_over = true
        break
      elsif draw?
        proclaim_draw
        game_over = true
        break
      end

      puts "- #{@comp.name}'s turn -".right_adjust($padding) + "\n\n"

      if scored? comp.move @table, @player
        proclaim_winner @comp
        @@games_time << Time.now - @start_time
        game_over = true
        break
      elsif draw?
        proclaim_draw
        game_over = true
        break
      end
    end

    new_game? if game_over
  end

  private

  def display_stats player, comp
    puts
    puts "<Wins>".right_adjust($padding)
    puts "#{player.name.capitalize}: #{player.scores[:wins]}".right_adjust($padding)
    puts "#{comp.name.capitalize}  : #{comp.scores[:wins]}".right_adjust($padding)
    puts
    puts "<Draws>".right_adjust($padding)
    puts "#{comp.scores[:draws]}".right_adjust($padding) + "\n" * 2
    @table.to_s
  end

  def scored? slot
    scoring_lines = []
    tld = [] # diagonal from top-left to bottom-right
    bld = [] # diagonal from bottom-left to top-right
    row_length = @table.slots.length
    row_times = row_length.times

    row_length.times { |n| tld << [n,n] }
    row_length.times { |n| bld << [n, (row_length -1) - n] }

    scoring_lines << @table.slots[slot.first]
    scoring_lines << row_times.inject([]) { |column, row| column << @table.slots[row][slot.last] }
    scoring_lines << row_times.inject([]) { |tld, n| tld << @table.slots[n][n] } if tld.include? slot
    scoring_lines << row_times.inject([]) { |bld, n| bld << @table.slots[n][(row_length - 1) - n] } if bld.include? slot

    scoring_lines.each { |line| return true if score line }
    false
  end

  def score line, points=0
    line.each { |slot| points += 1 if slot.eql? line.first }
    points == line.length
  end

  def proclaim_winner winner
    mins, secs = Game.count_game_time(Time.now - @start_time)

    puts
    puts "#{winner.name} wins!".right_adjust($padding)
    puts "This game lasted for - #{mins} min #{secs} sec.".right_adjust($padding)
    puts

    winner.increment_win
  end

  def proclaim_draw
    mins, secs = Game.count_game_time(Time.now - @start_time)

    puts "Game is a Draw!".right_adjust($padding)
    puts "This game lasted for - #{mins} min #{secs} sec.".right_adjust($padding)
    puts

    @player.increment_win
    @comp.increment_win
  end

  def draw?
    lines = []
    board = @table.slots
    board_val = board.length.times

    board.each { |row| lines << row }
    lines << board_val.inject([]) { |tld, n| tld << board[n][n] }
    lines << board_val.inject([]) { |bld, n| bld << board[n][(board.length - 1) - n] }
    board.length.times { |x| lines << board_val.inject([]) { |column, y| column << board[y][x] } }

    lines.each do |line|
      player_symbol = 0
      comp_symbol = 0

      line.each do |slot|
        break unless player_symbol.zero? || comp_symbol.zero?
        if slot.eql? @player
          player_symbol += 1
        elsif slot.eql? @comp
          comp_symbol += 1
        end
      end

      return false if player_symbol.zero? || comp_symbol.zero?
    end
    true
  end

  def new_game?
    loop do
      print "Do you want to play another game? (y/n) -> ".right_adjust($padding)
      case gets.downcase.chomp
      when "y"
        Game.new(@player, @comp, PlayerInput.choose_table_width).play_new
      when "n"
        if player.scores[:wins] > comp.scores[:wins]
          puts
          puts "#{player.name.upcase} is the winner!!".right_adjust($padding)
          puts
        elsif player.scores[:wins] < comp.scores[:wins]
          puts
          puts "#{comp.name.upcase} is the winner!!".right_adjust($padding)
          puts
        else
          puts
          puts "The game is a draw. :/".right_adjust($padding)
          puts
        end

        Game.game_time
        puts "Thank you for playing!".right_adjust($padding)
        puts
        exit
      else
        puts "Please answer with an appropriate letter.".right_adjust($padding)
      end
    end
  end
end
