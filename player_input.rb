class PlayerInput
  class <<self
    def choose_player_name
      puts
      puts "Enter your name:".right_adjust($padding)
      print ">> ".right_adjust($padding-3)
      loop do
        player_name = gets.chomp.strip
        if player_name.empty?
          puts "You must enter a name.".right_adjust($padding)
          print ">> ".right_adjust($padding-3)
        elsif player_name.size < 5
          puts "Your name must have at least five characters.".right_adjust($padding)
          print ">> ".right_adjust($padding-3)
        else
          return player_name
        end
      end
    end

    def choose_player_symbol
      puts "Enter a character you want to use:".right_adjust($padding)
      player_mark = ""
      loop do
        print ">> ".right_adjust($padding-3)
        player_mark = gets.chomp
        player_mark.length == 1 ? break : puts("Lenght of your symbol has to be 1.".right_adjust($padding))
      end
      return player_mark
    end

    def choose_table_width
      puts
      puts "Insert a dimension of a table you want to play with.".right_adjust($padding)
      puts "(ex. for a 3x3 table, with 9 slots, enter number -> 3)".right_adjust($padding)
      puts "You can choose 3x3, 4x4 or 5x5 board.".right_adjust($padding)

      loop do
        puts
        print ">> ".right_adjust($padding-2)
        t = gets.chomp.to_i
        if (3..5).include? t
          return t
        else
          puts
          puts "Invalid value. Number must be three (3) or greater.".right_adjust($padding)
        end
      end
    end
  end
end
