class Table
  attr_reader :slots, :width

  def initialize width
    @width = width
    @slots = create_slots width
  end

  def to_s
    table_as_string = ""
    self.slots.each do |row|
      table_as_string +=
        row.collect { |slot| slot.nil? ? " " : slot.symbol }
          .join(" | ") + "\n"
    end

    puts "--- board ---".right_adjust($padding)
    puts
    table_as_string.split("\n").each { |row| puts row.right_adjust($padding)}
  end

  private

  def create_slots width
    width.times.inject [] { |table| table << Array.new(width, nil) }
  end
end
